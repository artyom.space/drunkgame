var express = require('express');
var path = require('path'); //used for file path

var app = express();
app.use(express.static('src'));

var server = app.listen(3000, function() {
  console.log('Listening on port %d', server.address().port);
});
