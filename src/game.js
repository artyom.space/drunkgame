/*
Drunk walking game
Pixi.js as framework, 1 player
25.09.2019

UPD: 26.01.2020 - Multi-skins mode
UPD: 28.01.2020 - 2 players mode
*/

function DrunkGame(contentWrap, adminWrap) {
	// Config
	let app; // global main object
	let hero = [{},{}], metersCounter, input = 0, input1 = 0; // players
	let kSize = 1;
	let status = 'promo';
	const elements = {
		background: null,
		boom: null,
		light: null,
	};
	let walkingFrames = [], walkingFrames1 = [];

	// Важно! Нужно актуализировать CSS-ку для добавления информации по фонам и лого скина
	const skins = {
		'newyear': 'Новый год',
		'circus': 'Акробат в цирке'
	};

	let currentSkin = 'newyear';
	let currentPlayers = 'one'; // one, two

	// const foldPrefix = '/TehnoFun/core/collekt/';
	const foldPrefix = '';

	let sounds = {};

	// Aliases
	let Application = PIXI.Application,
		loader = PIXI.loader,
		resources = PIXI.loader.resources,
		Sprite = PIXI.Sprite,
		Rectangle = PIXI.Rectangle,
		TextureCache = PIXI.utils.TextureCache;

	// Controlling
	let V = 0, V1 = 0; // скорость
	let A = 0, A1 = 0; //ускорение игрока
	let GD = 1, GD1 = -1; // направление G
	let G = 0.1, G1 = 0.1; // начальное G
	let GA = 0.0003, GA1 = 0.0003; // ускорение для G
	let X = 0, X1 = 0;

	let globalCounter = 0;

	// PIXI.utils.skipHello(); // For remove banner

	// Init Pixi
	const type = PIXI.utils.isWebGLSupported() ? "WebGL" : "canvas";

	$(document).on('input', '#' + adminWrap + ' input.range', function() {
		input = parseInt($(this).val());
	});
	$(document).on('input', '#' + adminWrap + ' input.range1', function() {
		input1 = parseInt($(this).val());
	});

	// Загрузка начальных материалов
	this.Load_Game = function() {
		console.log('Load game')

		app = new Application({
			transparent: true,
			forceCanvas: true,
			// antialias: true
		});
		contentWrapper = document.getElementById(contentWrap);
		contentWrapper.appendChild(app.view);
		contentWrapper.insertAdjacentHTML('beforeend', '<div class="drunkSkinWrapper" data-players="'+currentPlayers+'" data-skin="'+currentSkin+'"><div class="drunkBack"></div><div class="drunkRoad"><b></b></div><div class="drunkStreetLeft streets"><b></b></div><div class="drunkStreetRight streets"><b></b></div><div class="drunkGradient"></div><div class="drunkPeoples"><b></b><b></b><b></b><b></b><b></b><b></b><b></b><b></b><b></b><b></b></div><div class="drunkBackMask"></div><div class="drunkLogo" data-skin="'+currentSkin+'"></div></div>');

		adminWrapper = document.getElementById(adminWrap);
		
		// Select выбора скина на основе переменной skins
		let selectSkins = '<label>Скин: <select id="drunkSkin" name="skin">';
		Object.keys(skins).forEach(skinFolder => {
			if (currentSkin == skinFolder) {
				selectSkins += '<option value="'+skinFolder+'" selected>'+skins[skinFolder]+'</option>';
			} else {
				selectSkins += '<option value="'+skinFolder+'">'+skins[skinFolder]+'</option>';
			}
		});
		selectSkins += '</select></label><br>';

		adminWrapper.innerHTML = '<input type="range" class="range" min="-10" max="10" value="0"/><br><input type="range" class="range1" min="-10" max="10" value="0"/><br>'+selectSkins+'<label>Игроки: <select class="drunkPlayers" name="drunkPlayers"><option selected value="one">Один игрок</option><option value="two">Два игрока</option></select></label><br><button id="startDrunkIntro">Интро звук<button><br><button id="startDrunkgame">Старт<button><br><button id="resetDrunkgame">Сбросить<button>';

		$("#startDrunkIntro").click(function(){
			sounds.intro.loop = true;
			sounds.intro.play();

			sounds.go.pause();
			sounds.end.pause();
			sounds.go.currentTime = 0;
			sounds.end.currentTime = 0;
		});

		// Skin changer
		$("#drunkSkin").change(function(){
			if (currentSkin == 'newyear') {
				hero[0].onFloor.destroy();
				hero[1].onFloor.destroy();
				elements.boom.destroy();
			} else {
				elements.light.alpha = 0;
			}
			hero[0].container.destroy();
			hero[1].container.destroy();
			metersCounter.destroy();

			currentSkin = $(this).val();
			$(".drunkSkinWrapper").attr('data-skin', currentSkin);

			loadImages(currentSkin);
		});

		// Players counter changer
		$(".drunkPlayers").change(function(){
			console.log($(this).val())
			if (currentSkin == 'newyear') {
				hero[0].onFloor.destroy();
				hero[1].onFloor.destroy();
				elements.boom.destroy();
			} else {
				elements.light.alpha = 0;
			}
			hero[0].container.destroy();
			hero[1].container.destroy();
			metersCounter.destroy();

			currentPlayers = $(this).val();
			$(".drunkSkinWrapper").attr('data-players', currentPlayers);

			loadImages(currentSkin);
		});

		$("#resetDrunkgame").click(function(){
			sounds.intro.loop = true;
			sounds.intro.play();

			sounds.go.pause();
			sounds.end.pause();
			sounds.go.currentTime = 0;
			sounds.end.currentTime = 0;

			resetHero();
		});
		$("#startDrunkgame").click(function(){
			status = 'play';
			$(".drunkLogo").addClass('hidden');
			metersCounter.alpha = 1;

			sounds.go.loop = true;
			sounds.go.play();

			sounds.intro.pause();
			sounds.end.pause();
			sounds.intro.currentTime = 0;
			sounds.end.currentTime = 0;
		});

		app.renderer.backgroundColor = 0xDEDEDE;
		app.renderer.view.style.position = "absolute";
		app.renderer.view.style.display = "block";
		app.renderer.autoResize = true;
		app.renderer.resize(contentWrapper.offsetWidth, contentWrapper.offsetHeight);

		window.onresize = function() {
			app.renderer.resize(contentWrapper.offsetWidth, contentWrapper.offsetHeight);
			resizeObjects();
		}

		// Загружаем нужные изображения	
		loadImages(currentSkin);
	}

	// Загрузка текстур в зависимости от скина
	function loadImages(skin) {
		loader.reset();
		walkingFrames = [];
		walkingFrames1 = [];
		if (skin == 'newyear') {
			// Для новогоднего скина есть исключения
			// Анимация ходьбы только для новогоднего скина
			for (let i=1; i < 7; i++) {
				const texture = PIXI.Texture.fromImage(foldPrefix + 'skins/newyear/' +i+ '.png');
				walkingFrames.push(texture);
			}
			// Грузим изображения в кэш
			loader
				.add('back', foldPrefix + 'skins/' + currentSkin + '/background.png')
				.add('boom', foldPrefix + 'skins/' + currentSkin + '/boom.png')
				.add('onFloor', foldPrefix + 'skins/newyear/onFloor.png')
				.add('onFloor1', foldPrefix + 'skins/newyear/1onFloor.png')
				.add('body', foldPrefix + 'skins/' + currentSkin + '/body.png')
				.add('body1', foldPrefix + 'skins/' + currentSkin + '/1body.png')
				.add('lHand', foldPrefix + 'skins/' + currentSkin + '/leftHand.png')
				.add('lHand1', foldPrefix + 'skins/' + currentSkin + '/1leftHand.png')
				.add('rHand', foldPrefix + 'skins/' + currentSkin + '/rightHand.png')
				.add('rHand1', foldPrefix + 'skins/' + currentSkin + '/1rightHand.png')
				.load(setup);
		} else {
			// Анимация ходьбы для цирка
			for (let i=0; i < 4; i++) {
				const texture = PIXI.Texture.fromImage(foldPrefix + 'skins/circus/body' +i+ '.png');
				const texture1 = PIXI.Texture.fromImage(foldPrefix + 'skins/circus/1body' +i+ '.png');
				walkingFrames.push(texture);
				walkingFrames1.push(texture1);
			}
			// Грузим изображения в кэш
			loader
				.add('back', foldPrefix + 'skins/' + currentSkin + '/back.png')
				.add('light', foldPrefix + 'skins/' + currentSkin + '/light.png')
				.add('rope', foldPrefix + 'skins/' + currentSkin + '/rope.png')
				.add('body', foldPrefix + 'skins/' + currentSkin + '/body.png')
				.add('lHand', foldPrefix + 'skins/' + currentSkin + '/leftHand.png')
				.add('rHand', foldPrefix + 'skins/' + currentSkin + '/rightHand.png')
				.add('lHand1', foldPrefix + 'skins/' + currentSkin + '/1leftHand.png')
				.add('rHand1', foldPrefix + 'skins/' + currentSkin + '/1rightHand.png')
				.load(setup);
		}
		// После загрузки вызываем функцию setup
	}

	// Очищаем память, выгружаем приложение и заметаем следы
	this.destroy = function() {
		app.destroy(true, true);
		loader.reset();
		document.getElementById(contentWrap).innerHTML = "";
		document.getElementById(adminWrap).innerHTML = "";
	}

	function resizeObjects() {
		kSize = elements.background.width / app.renderer.width;
		hero[0].container.width = hero[0].container.originalWidth / kSize;
		hero[0].container.height = hero[0].container.originalHeight / kSize;
		hero[0].container.y = app.renderer.height * 0.9;
		hero[1].container.width = hero[1].container.originalWidth / kSize;
		hero[1].container.height = hero[1].container.originalHeight / kSize;
		hero[1].container.y = app.renderer.height * 0.9;

		if (currentPlayers == "one") {
			hero[0].container.x = app.renderer.width / 2;
			hero[1].alpha = 0;
		} else {
			hero[0].container.x = app.renderer.width * 0.25;
			hero[1].container.x = app.renderer.width * 0.75;
			hero[1].alpha = 1;
		}

		if (currentSkin == 'newyear') {
			hero[0].onFloor.width = hero[0].onFloor.originalWidth / kSize;
			hero[0].onFloor.height = hero[0].onFloor.originalHeight / kSize;
			hero[0].onFloor.y = app.renderer.height * 0.9;

			elements.boom.width = elements.boom.originalWidth / kSize;
			elements.boom.height = elements.boom.originalHeight / kSize;
			elements.boom.alpha = 0;
			elements.boom.y = app.renderer.height * 0.8;
		}
		if (currentSkin == 'circus') {
			elements.light.width = app.renderer.width;
			elements.light.height = app.renderer.height;
			elements.light.y = 0;
			elements.light.x = app.renderer.width * 0.5;
		}

		if (currentPlayers == "one") {
			metersCounter.position.set(app.renderer.width * 0.1, app.renderer.height * 0.5);
		} else {
			metersCounter.position.set(app.renderer.width * 0.5, app.renderer.height * 0.05);
		}
	}

	function setHero(hero, number = 1) {
		hero = {};
		hero.number = number;
		if (currentSkin == 'newyear') {
			if (number == 1) {
				hero.body = new Sprite(TextureCache["body"]);
			} else {
				hero.body = new Sprite(TextureCache["body1"]);
			}
		} else {
			if (number == 1) {
				hero.body = new PIXI.extras.AnimatedSprite(walkingFrames);
			} else {
				hero.body = new PIXI.extras.AnimatedSprite(walkingFrames1);
			}
			hero.body.animationSpeed = 0.25;
			hero.body.play();
		}
		hero.body.anchor.x = 0;
		hero.body.anchor.y = 0;
		if (currentSkin == 'newyear') {
			hero.body.x = 333; // old 333
		} else {
			hero.body.x = 220; // old 333
		}
		hero.body.y = 0;

		if (number == 1) {
			hero.lHand = new Sprite(TextureCache["lHand"]);
			hero.rHand = new Sprite(TextureCache["rHand"]);
		} else {
			hero.lHand = new Sprite(TextureCache["lHand1"]);
			hero.rHand = new Sprite(TextureCache["rHand1"]);
		}

		if (currentSkin == 'newyear') {
			hero.lHand.anchor.x = 0.911;
			hero.lHand.anchor.y = 0.2;
			hero.lHand.x = 357; // app.renderer.width / 2 - 45;
			hero.lHand.y = 173;

			hero.rHand.anchor.x = 0.08495145631068;
			hero.rHand.anchor.y = 0.263157894736842;
			hero.rHand.x = 450;
			hero.rHand.y = 178;
		} else {
			hero.lHand.anchor.x = 0.9361;
			hero.lHand.anchor.y = 0.481;
			hero.lHand.x = 297; // app.renderer.width / 2 - 45;
			hero.lHand.y = 135;

			hero.rHand.anchor.x = 0.07659;
			hero.rHand.anchor.y = 0.481;
			hero.rHand.x = 377;
			hero.rHand.y = 135;
		}

		hero.container = new PIXI.Container();

		if (currentSkin == 'newyear') {
			hero.foots = new PIXI.extras.AnimatedSprite(walkingFrames);
			hero.foots.anchor.x = 0.5;
			hero.foots.anchor.y = 0;
			hero.foots.x = 393;
			hero.foots.y = 320;
			hero.foots.animationSpeed = 0.25;
			hero.foots.play();
			hero.container.addChild(hero.foots);
		}

		hero.container.addChild(hero.lHand);
		hero.container.addChild(hero.rHand);
		hero.container.addChild(hero.body);

		if (currentSkin == 'newyear') {
			hero.container.pivot.set(hero.container.width/2, hero.container.height);
		} else {
			hero.container.pivot.set(hero.container.width*0.7, hero.container.height);
		}
		hero.container.originalWidth = hero.container.width;
		hero.container.originalHeight = hero.container.height;
		hero.container.width = hero.container.width / kSize;
		hero.container.height = hero.container.height / kSize;
		hero.container.x = app.renderer.width / 2;
		hero.container.y = app.renderer.height * 0.9;

		if (number == 1) {
			hero.onFloor = new Sprite(TextureCache["onFloor"]);
		} else {
			hero.onFloor = new Sprite(TextureCache["onFloor1"]);
		}
		hero.onFloor.anchor.x = 0.5;
		hero.onFloor.anchor.y = 1;
		hero.onFloor.originalWidth = hero.onFloor.width;
		hero.onFloor.originalHeight = hero.onFloor.height;
		hero.onFloor.width = hero.onFloor.width / kSize;
		hero.onFloor.height = hero.onFloor.height / kSize;
		hero.onFloor.y = app.renderer.height * 0.9;

		hero.onFloor.alpha = 0;

		app.stage.addChild(hero.onFloor);
		app.stage.addChild(hero.container);

		return hero;
	}

	// случайное число от min до max
	function getRandom(min, max) {
	  return Math.floor(Math.random() * (max - min) + min);
	}

	function cleanSetup() {
		console.log('setup');
		if (Object.keys(sounds).length > 0) {
			sounds.intro.pause();
			sounds.go.pause();
			sounds.end.pause();
			sounds.intro.currentTime = 0;
			sounds.go.currentTime = 0;
			sounds.end.currentTime = 0;
			sounds.intro = null;
			sounds.go = null;
			sounds.end = null;
		}
		sounds = {
			'intro': new Audio(foldPrefix + 'skins/' + currentSkin + '/sounds/intro.mp3'),
			'go': new Audio(foldPrefix + 'skins/' + currentSkin + '/sounds/go.mp3'),
			'end': new Audio(foldPrefix + 'skins/' + currentSkin + '/sounds/end.mp3'),
		};

		// Установка размера элементов производится на основе изменения
		// размеров бэкграунда по сравнению с оригинальным
		elements.background = new Sprite(TextureCache["back"]);
		kSize = elements.background.width / app.renderer.width;

		// Set meters counter
		metersCounter = new PIXI.Text('0.00');
		metersCounter.style = {fill: "white", fontSize: '6em', stroke: "#d11c21", letterSpacing: 3, strokeThickness: 10, lineJoin: 'round', align: "center"};
		metersCounter.anchor.x = 0.5;
		if (currentPlayers == "one") {
			metersCounter.position.set(app.renderer.width * 0.1, app.renderer.height * 0.5);
		} else {
			metersCounter.position.set(app.renderer.width * 0.5, app.renderer.height * 0.05);
		}
		metersCounter.text = '1';
		app.stage.addChild(metersCounter);

		// Prepare Hero
		hero[0] = setHero(hero[0],1);
		hero[1] = setHero(hero[1],2);
		
		// Check qty of players
		if (currentPlayers == "two") {
			console.log('curr',currentPlayers)
			hero[0].container.x = app.renderer.width * 0.25;
			hero[1].container.x = app.renderer.width * 0.75;
			hero[1].container.alpha = 1;
		} else {
			console.log('curr',currentPlayers)
			hero[1].container.alpha = 0;
		}

		if (currentSkin == 'newyear') {
			// Add 'boom' animation
			elements.boom = new Sprite(TextureCache["boom"]);
			elements.boom.originalWidth = elements.boom.width;
			elements.boom.originalHeight = elements.boom.height;
			elements.boom.anchor.x = 0.5;
			elements.boom.anchor.y = 0.5;
			elements.boom.width = elements.boom.originalWidth / kSize;
			elements.boom.height = elements.boom.originalHeight / kSize;
			elements.boom.y = app.renderer.height * 0.8;
			elements.boom.x = app.renderer.width * 0.7; // .7 or .25
			elements.boom.scale.x = 0;
			elements.boom.scale.y = 0;
			elements.boom.grow = true;
			app.stage.addChild(elements.boom);
		} else {
			if (elements.light == null) {
				elements.light = new Sprite(TextureCache["light"]);
				elements.light.originalWidth = elements.light.width;
				elements.light.originalHeight = elements.light.height;
				elements.light.anchor.x = 0.5;
				elements.light.anchor.y = 0;
				elements.light.width = app.renderer.width;
				elements.light.height = app.renderer.height;
				elements.light.y = 0;
				elements.light.x = app.renderer.width * 0.5;
				elements.light.scale.x = 1;
				elements.light.scale.y = 1;
				app.stage.addChild(elements.light);
			} else {
				elements.light.alpha = 1;
			}
		}

	}

	let ticker = null;
	// Setup stage
	function setup() {
		cleanSetup();

		state = play;

		if (ticker == null) {
			ticker = app.ticker.add(delta => gameLoop(delta));
		}
	}

	// Loop
	function gameLoop(delta) {
		state(delta);
	}

	function toDegrees (angle) {
		return angle * (180 / Math.PI);
	}
	function toRadians (angle) {
	  return angle * (Math.PI / 180);
	}

	function resetHero() {
		V=0; V1=0;  
		A=0; A1=0;  
		GD=1; GD1=1;  
		G=0.1; G1=-0.1;  
		GA=0.0003; GA1=0.0003;  
		X=0; X1=0;
		$(".streets").removeClass('paused');
		$(".drunkRoad").removeClass('paused');
		$(".drunkPeoples").removeClass('paused');
		hero[0].onFloor.alpha = 0;
		hero[0].container.alpha = 1;
		hero[0].container.rotation = toRadians(0);
		hero[0].lHand.rotation = toRadians(-15);
		hero[0].rHand.rotation = toRadians(15);
		hero[0].container.y = app.renderer.height * 0.9;
		hero[1].onFloor.alpha = 0;
		if (currentPlayers == 'two') {
			hero[1].container.alpha = 1;
		}
		hero[1].container.rotation = toRadians(0);
		hero[1].lHand.rotation = toRadians(-15);
		hero[1].rHand.rotation = toRadians(15);
		hero[1].container.y = app.renderer.height * 0.9;
		metersCounter.text = '0.00';

		if (currentSkin == 'newyear') {
			elements.boom.grow = true;
		}

		$(".drunkLogo").removeClass('hidden');

		status = 'promo';
	}

	function playerFall(direction, h = hero[0]) {
		status = 'gameover';

		// Sounds check
		if (!sounds.go.paused) {
			sounds.end.loop = false;
			sounds.end.play();

			sounds.intro.pause();
			sounds.go.pause();
			sounds.intro.currentTime = 0;
			sounds.go.currentTime = 0;
		}

		if (direction == 'left') {
			// Падаем налево
			if (currentSkin == 'newyear') {
				h.onFloor.scale.x = -1 / kSize;
				if (currentPlayers == "one") {
					h.onFloor.x = app.renderer.width * 0.3;
					elements.boom.x = app.renderer.width * 0.25;
				} else {
					if (h.number == 1) {
						h.onFloor.x = app.renderer.width * 0.2;
						elements.boom.x = app.renderer.width * 0.25;
					} else {
						h.onFloor.x = app.renderer.width * 0.65;
						elements.boom.x = app.renderer.width * 0.7;
					}
				}
			}
		}
		if (direction == 'right') {
			// Падаем направо
			if (currentSkin == 'newyear') {
				if (currentPlayers == "one") {
					h.onFloor.x = app.renderer.width * 0.7;
					elements.boom.x = app.renderer.width * 0.7;
				} else {
					if (h.number == 1) {
						h.onFloor.x = app.renderer.width * 0.3;
						elements.boom.x = app.renderer.width * 0.25;
					} else {
						h.onFloor.x = app.renderer.width * 0.75;
						elements.boom.x = app.renderer.width * 0.7;
					}
				}
			}
		}

		// Общие действия при падении
		if (currentSkin == 'newyear') {
			$(".streets").addClass('paused');
			$(".drunkRoad").addClass('paused');
			// Boom movement
			elements.boom.rotation += .1;
			if (elements.boom.scale.x >= 1){
				elements.boom.grow = false;
			}
			if (elements.boom.scale.x < 1 && elements.boom.grow == true) {
				elements.boom.scale.x += 0.05;
				elements.boom.scale.y += 0.05;
			}
			if (elements.boom.scale.x >= 0 && elements.boom.grow == false) {
				elements.boom.scale.x -= 0.05;
				elements.boom.scale.y -= 0.05;
			}
		} else {
			$(".drunkPeoples").addClass('paused');
		}
	}

	function play(delta) {
		// Light animation
		if (currentSkin == 'circus' && elements.light != null) {
			globalCounter += 1;
			elements.light.scale.x = Math.sin(toRadians(globalCounter))*0.1+0.9;
			elements.light.scale.y = Math.sin(toRadians(globalCounter))*0.1+0.9;
		}

		if (Math.abs(X/100) > 60 || (Math.abs(X1/100) > 60 && currentPlayers == 'two')) {
			// 1st player
			if (Math.abs(X/100) > 60) {
				// Player is fall
				if (currentSkin == 'newyear') {
					hero[0].onFloor.alpha = 1;
					hero[0].container.alpha = 0;
				} else {
					// If show on screen
					hero[0].container.y = hero[0].container.y + hero[0].container.height*0.05;
				}
				if (X > 0) {
					playerFall('right', hero[0]);
				} else {
					playerFall('left', hero[0]);
				}				
			}
			// 2nd player
			if (Math.abs(X1/100) > 60 && currentPlayers == 'two') {
				// Player is fall
				if (currentSkin == 'newyear') {
					hero[1].onFloor.alpha = 1;
					hero[1].container.alpha = 0;
				} else {
					// If show on screen
					hero[1].container.y = hero[1].container.y + hero[1].container.height*0.05;
				}
				if (X1 > 0) {
					playerFall('right', hero[1]);
				} else {
					playerFall('left', hero[1]);
				}	
			}
		} else {
			// Player still walkin'
			if (status == 'play') {
				metersCounter.alpha = 1;

				A = -input/5; A1 = -input1/5;
				V = V+(G*GD-A); V1 = V1+(G1*GD1-A1);
				G = G+GA; G1 = G1+GA1;
				Xv = X; Xv1 = X1;
				X = X+V; X1 = X1+V1;

				hero[0].container.rotation = toRadians(X/100);
				hero[0].lHand.rotation = toRadians(X/150 - 15);
				hero[0].rHand.rotation = toRadians(X/150 + 15);

				hero[1].container.rotation = toRadians(X1/100);
				hero[1].lHand.rotation = toRadians(X1/150 - 15);
				hero[1].rHand.rotation = toRadians(X1/150 + 15);


				if((Xv>0)&&(X<0)) { GD=-1; } if((Xv1>0)&&(X1<0)) { GD1=-1; }
				if((Xv<0)&&(X>0)) { GD=1; } if((Xv1<0)&&(X1>0)) { GD1=1; } 

				// Increase meters
				metersCounter.text = (parseFloat(metersCounter.text) + .01).toFixed(2);
			}
			if (status == 'promo') {
				metersCounter.alpha = 0;
			}
		}
	}
}